import React from 'react';
import Map from './Map';

const App = () => {
  const location = { lat: 40.7128, lng: -74.006 };

  return (
    <div>
      <h1>My Map</h1>
      <Map location={location} />
    </div>
  );
};

export default App;
